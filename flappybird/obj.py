import pygame


class Obj(pygame.sprite.Sprite):
    def __init__(self, img, x, y, *groups):
        super().__init__(*groups)

        self.image = pygame.image.load(img)
        self.rect = self.image.get_rect()
        self.rect[0] = x
        self.rect[1] = y


class Pipe(Obj):
    def __init__(self, img, x, y, *groups):
        super().__init__(img, x, y, *groups)

    def update(self, *args, **kwargs) -> None:
        self.move()

    def move(self):
        self.rect[0] -= 3

        if self.rect[0] <= -100:
            self.kill()
        
    
class Coin(Obj):
    def __init__(self, img, x, y, *groups):
        super().__init__(img, x, y, *groups)

        self.tick = 0

    def update(self, *args, **kwargs) -> None:
        self.move()
        self.anim()

    def move(self):
        self.rect[0] -= 3

        if self.rect[0] <= -100:
            self.kill()
    
    def anim(self):
        self.tick = (self.tick + 1) % 6
        self.image = pygame.image.load(f'assets/{str(self.tick)}.png')


class Bird(Obj):
    def __init__(self, img, x, y, *groups):
        super().__init__(img, x, y, *groups)

        self.tick = 0
        self.vel = 3
        self.grav = 1
        self.pts = 0

        self.play = True

    def update(self, *args, **kwargs) -> None:
        self.move()
        self.anim()

    def move(self):
        key = pygame.key.get_pressed()
        
        self.vel += self.grav
        self.rect[1] += self.vel

        if self.vel >= 5:
            self.vel = 5
        
        if self.play:
            if key[pygame.K_SPACE]:
                self.vel -= 5
        
        if self.rect[1] >= 440:
            self.rect[1] = 440
        elif self.rect[1] <= 0:
            self.rect[1] = 0
            self.vel = 4

    def colision_pipes(self, group):
        col = pygame.sprite.spritecollide(self, group, False)

        if col:
            self.play = False
        
    def colision_coin(self, group):
        col = pygame.sprite.spritecollide(self, group, True)

        if col:
            self.pts += 1

    def anim(self):
        self.tick = (self.tick + 1) % 4
        self.image = pygame.image.load(f'assets/bird{str(self.tick)}.png')
        
    
class Text:
    def __init__(self, size, text):
        self.font = pygame.font.Font('assets/font/font.ttf', size)
        self.render = self.font.render(text, True, (255, 255, 255))
    
    def draw(self, window, x, y):
        window.blit(self.render, (x, y))
    
    def text_update(self, text):
        self.render = self.font.render(text, True, (255, 255, 255))