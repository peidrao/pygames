import pygame

pygame.init()

window = pygame.display.set_mode([1280, 720])
title = pygame.display.set_caption('Soccer Pong')

win = pygame.image.load('/home/peidrao/study/pygames/pong/assets/win.png')

field = pygame.image.load('/home/peidrao/study/pygames/pong/assets/field.png')

player1 = pygame.image.load('/home/peidrao/study/pygames/pong/assets/player1.png')
player1_y = 310
player1_moveup = False
player1_movedown = False

player2 = pygame.image.load('/home/peidrao/study/pygames/pong/assets/player2.png')
player2_y = 310

score_player1 = 0
score_player2 = 7

score1_img = pygame.image.load('/home/peidrao/study/pygames/pong/assets/score/0.png')
score2_img = pygame.image.load('/home/peidrao/study/pygames/pong/assets/score/0.png')

ball = pygame.image.load('/home/peidrao/study/pygames/pong/assets/ball.png')
ball_x = 617
ball_y = 317
ball_dir = -15
ball_dir_y = 1

def move_play():
	global player1_y

	if player1_moveup:
		player1_y -= 10
	else:
		player1_y += 0

	if player1_movedown:
		player1_y += 10
	else:
		player1_y += 0

	if player1_y <= 0:
		player1_y = 0
	elif player1_y >= 575:
		player1_y = 575

def move_ball():
	global ball_x
	global ball_y
	global ball_dir
	global ball_dir_y
	global score_player1
	global score_player2
	global score1_img
	global score2_img

	ball_x += ball_dir
	ball_y += ball_dir_y

	if ball_x < 120:
		if player1_y < ball_y + 23:
			if player1_y + 146 > ball_y:
				ball_dir *= -1
	
	if ball_x > 1100:
		if player2_y < ball_y + 23:
			if player2_y + 146 > ball_y:
				ball_dir *= -1
	
	if ball_y > 685 :
		ball_dir_y *= -1
	elif ball_y <= 0:
		ball_dir_y *= -1
	
	if ball_x < -50:
		ball_x = 617
		ball_y = 337
		ball_dir_y *= -1
		ball_dir *= -1
		score_player2 += 1
		score2_img = pygame.image.load(f'/home/peidrao/study/pygames/pong/assets/score/{str(score_player2)}.png')
	elif ball_x > 1320:
		ball_x = 617
		ball_y = 337
		ball_dir_y *= -1
		ball_dir *= -1
		score_player1 += 1
		score1_img = pygame.image.load(f'/home/peidrao/study/pygames/pong/assets/score/{str(score_player1)}.png')

def move_player2():
	global player2_y
	player2_y = ball_y


def draw():
	if score_player1 or score_player2 < 9:
		window.blit(field, (0, 0))
		window.blit(player1, (50, player1_y))
		window.blit(player2, (1150, player2_y))
		window.blit(ball, (ball_x, ball_y))
		window.blit(score1_img, (500, 50))
		window.blit(score2_img, (710, 50))
		move_ball()
		move_play()
		move_player2()
	else:
		window.blit(win, (300, 330))


loop = True
while loop:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			loop = False
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_w:
				player1_moveup = True
			if event.key == pygame.K_s:
				player1_movedown = True
		if event.type == pygame.KEYUP:
			if event.key == pygame.K_w:
				player1_moveup = False
			if event.key == pygame.K_s:
				player1_movedown = False

	draw()
	pygame.display.update()