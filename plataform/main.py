import pygame
from game import Game


class Main:

    def __init__(self, sizex, sizey, title):
        pygame.font.init()
        self.window = pygame.display.set_mode([1280, 720])
        self.title = pygame.display.set_caption(title)

        self.loop = True
        self.fps = pygame.time.Clock()
        self.game = Game()
        

    def draw(self):
        self.game.draw(self.window)

    def events(self):
        for events in pygame.event.get():
            if events.type == pygame.QUIT:
                pygame.quit()
                self.loop = False

    def update(self):
        while self.loop:
            self.draw()
            self.events()
            pygame.display.update()
            self.fps.tick(30)


game = Main(360, 640, "Plataform")
game.update()
